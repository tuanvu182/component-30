<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<div class="c-list">

  <div class="c-list__showcase">
    <div class="c-card1">
      <img src="../assets/img/component30/shikaoi_brand01.png" alt="brand image #1">
      <div class="c-list__content">
        <p class="c-title3">肥沃な大地で生産する安全な農作物</p>
        <p class="c-text1">テキストテキストテキストテキストテキストテキストテキスト テキストテキストテキストテキストテキスト</p>
      </div>
    </div>

    <div class="c-card1">
      <img src="../assets/img/component30/shikaoi_brand02.png" alt="brand image #2">
      <div class="c-list__content">
        <p class="c-title3">北海道を代表する高品質の牛乳</p>
        <p class="c-text1">テキストテキストテキストテキストテキストテキストテキスト テキストテキストテキストテキストテキスト</p>
      </div>
    </div>

    <div class="c-card1">
      <img src="../assets/img/component30/shikaoi_brand03.png" alt="brand image #3">
      <div class="c-list__content">
        <p class="c-title3">信頼の「鹿追ブランド」牛肉・豚肉</p>
        <p class="c-text1">テキストテキストテキストテキストテキストテキストテキスト テキストテキストテキストテキストテキスト</p>
      </div>
    </div>
  </div>
</div>
